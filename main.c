#include <freertos/include/FreeRTOS.h>
#include <freertos/include/task.h>
#include <dbg/debug.h>
#include <dbg/cli.h>
#include "board_io.h"
#include <qei/qei.h>
#include <adc/adc.h>
#include <pwm/pwm.h>
#include <stdio.h>
#define TeMs 10 // temps d'�chantillonnage 10ms

static unsigned int count = 0 ;
static int shouldStop = 1 ;
int commandeMotor = 0;
int my_speedConsigne=0;
float Error=0;
int Kp=30;
int Ki=3;

void toggle_led()
{
    static int state = 0 ;

    trc_printf(2, "%04x------------LedTask------------",count++);
    if (state)
    {
        board_setLed(1);
    }
    else
    {
        board_setLed(0);
    }
    state = !state ;
}

void ledTask( void *param )
{
    for( ;; )
    {
        vTaskDelay(100);
        toggle_led() ;
    }
}
///////////////////////////////////////////////////////Commande motor en duty_cycle//////////////////////////////////////////
/*void setMyGlobalVariable_dbg (int value)
{
     commandeMotor=value;//recoit des valeurs entre 0 et 255
}
REGISTER_CLI_INT(setMyGlobalVariable_dbg)*/

void Vitesse_consigne (int value)
{
     my_speedConsigne=value;
}
REGISTER_CLI_INT(Vitesse_consigne)

void Kp_controller (int value)
{
     Kp=value;
}
REGISTER_CLI_INT(Kp_controller)

void Ki_controller (int value)
{
     Ki=value;
}
REGISTER_CLI_INT(Ki_controller)
///////////////////////////////////////////commande moteur en BO////////////////////////////////////////////////////

/*void Motor( void *param )
{

    for( ;; )
    {
        vTaskDelay(100);
        board_setRightMotorPwm(commandeMotor);

    }
}*/

void Asservissement_M (void *param)
{   float edge1;float edge2;float diffEdge;float Speed;float Vitesse_mesure;float Somme_ERROR=0;
     for( ;; )
     {
    edge1=board_getMotorEnc(1);
    vTaskDelay(TeMs);
    edge2=board_getMotorEnc(1);
    diffEdge=(edge2-edge1);
    //////////////////////////////////gestion de l'overflow/////////////////////////////////////////////////////////////////
    if(diffEdge<-32768)
    {
    diffEdge=edge2+(65535-edge1);
    }
    else if(diffEdge>32768)
    {
    diffEdge= -(edge1+(65535-edge2));
    }
    ///////////////////////////////Calcule vitesse MOTOR/////////////////////////////////////////////////////////////////////
    Speed =((diffEdge/2652)*100);//en tr/s
    Vitesse_mesure = Speed*6,28;//en rad/s
    Error = my_speedConsigne - Vitesse_mesure;
    Somme_ERROR=Somme_ERROR+Error/100;
    if(Somme_ERROR>35)
    {Somme_ERROR=30;}
    if(Somme_ERROR<-30)
    {Somme_ERROR=-35;}
    trc_printf(4,"somme erreur==%0,2f",Somme_ERROR);
    commandeMotor = Kp*Error+Ki*Somme_ERROR+128;//comandeMotor doit etre sous forme de Dutycycle
    ////////////////////////////////////limitation vitesse max et min en duty_cycle/////////////////////////////////////////
    if(commandeMotor>255)
    {
    commandeMotor=255;
    }
    else if (commandeMotor<0)
    {
    commandeMotor=0;
    }

    board_setRightMotorPwm(commandeMotor);//commande moteur est le duty cycle qui commande le moteur
    trc_printf(3,"Consigne: %d rad/s==Erreur: %0.1f  en rad/s==Vitesse %0.1f en rad/s",my_speedConsigne,Error,Vitesse_mesure);

    }

}





int main()
{
    dbg_init(1);
    cli_init(1);
    adc_init(ADC_IDX);
    //Initiate Board
    board_idGpioInit();
    board_ledGpioInit();
    board_motorGpioInit();
    board_qeiGpioInit();
    // instantiate the QEI - encoders
    qei_init(LEFT_MOTOR_ENCODER_TIM_IDX); //3
    qei_init(RIGHT_MOTOR_ENCODER_TIM_IDX); //4
    qei_init(LEFT_EXTERN_ENCODER_TIM_IDX); //8
    qei_init(RIGHT_EXTERN_ENCODER_TIM_IDX); //2

    // instantiate the PWM
    pwm_initChannel(LEFT_MOTOR_TIM_IDX, LEFT_MOTOR_CHANNEL1_IDX, 10, 1);  //IN1L -> TIM1_CH1N
    pwm_enableOutput(LEFT_MOTOR_TIM_IDX, LEFT_MOTOR_CHANNEL1_IDX, 1);
    pwm_initChannel(RIGHT_MOTOR_TIM_IDX, RIGHT_MOTOR_CHANNEL1_IDX, 10, 1);  //IN1R -> TIM2_CH3
    pwm_enableOutput(RIGHT_MOTOR_TIM_IDX, RIGHT_MOTOR_CHANNEL1_IDX, 0);

    pwm_initChannel(LEFT_MOTOR_TIM_IDX, LEFT_MOTOR_CHANNEL2_IDX , 10, 0);  //IN2L -> TIM1_CH2N
    pwm_enableOutput(LEFT_MOTOR_TIM_IDX, LEFT_MOTOR_CHANNEL2_IDX , 1);
    pwm_initChannel(RIGHT_MOTOR_TIM_IDX, RIGHT_MOTOR_CHANNEL2_IDX, 10, 0);  //IN2R -> TIM2_CH4
    pwm_enableOutput(RIGHT_MOTOR_TIM_IDX, RIGHT_MOTOR_CHANNEL2_IDX, 0);


    //instantiate the ADC
    //PA4 - ADC4 - Voltage
    board_enableAdcChannelPin(ADC_IDX, VOLTAGE_ADC_CHANNEL);
    //PC4 - ADC14 - Current Right Motor
    board_enableAdcChannelPin(ADC_IDX, RIGHT_MOTOR_CURRENT_ADC_CHANNEL);
    //PC5 - ADC15 - Current Left Motor
    board_enableAdcChannelPin(ADC_IDX, LEFT_MOTOR_CURRENT_ADC_CHANNEL);


    //Enable motor and set to neutral pwm (stop state)
    board_enableRight(1);
    board_enableLeft(1);
    pwm_setDuty(LEFT_MOTOR_TIM_IDX, LEFT_MOTOR_CHANNEL1_IDX, 128);
    pwm_setDuty(LEFT_MOTOR_TIM_IDX, LEFT_MOTOR_CHANNEL2_IDX, 128);
    pwm_setDuty(RIGHT_MOTOR_TIM_IDX, RIGHT_MOTOR_CHANNEL1_IDX, 128);
    pwm_setDuty(RIGHT_MOTOR_TIM_IDX, RIGHT_MOTOR_CHANNEL2_IDX, 128);

    //Init Tasks
    xTaskCreate(ledTask, "LED", 250, NULL, 1, NULL);
    //xTaskCreate(Motor, "Motor", 250, NULL,3, NULL );//bo
    xTaskCreate(Asservissement_M, "Asservissement", 250, NULL,2, NULL);//Bf

    vTaskStartScheduler();
}
